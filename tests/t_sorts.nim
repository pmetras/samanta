import unittest
import samanta

const
  Verbose = false
  #Verbose = true
  ## Set to `true` for more verbose test results

template dbg(v: varargs[untyped]) =
  ## Prints the `v` arguments for debugging.
  if Verbose: debugEcho(v)


suite "debug z3nim":
  test "int sort evaluation":
    smt:
      let a = Int("a")
      let b = Int("b")
      let s = Solver()
      s.assert a == 1
      s.assert b == 2 + 3   # Constant folding by Nim compiler
      if s.check() == True3:
        let model = s.getModel()
        check:
          model.evalInt(a) == 1
        check:
          model.evalInt(b) == 5


  test "sorts: mixed assignments":
    smt:
      let a = Bool("a")
      let b = Int("b")
      let c = Real("c")
      let d = Bv("d", 32)
      let s = Solver()
      expect SmtSyntaxError:
        s.assert a == 5       # Can't assign int value to bool
      expect SmtSyntaxError:
        s.assert b == false   # Can't assign bool value to int
      expect SmtSyntaxError:
        s.assert c == -12     # Can't assign int value to float
      expect SmtSyntaxError:
        s.assert a == 8.76    # Can't assign float value to int
      #expect SmtError:
      #  s.assert d == 64      # Can't assign int value to bitvec
      #expect SmtError:
      #  s.assert d == 3.14    # Can't assign float value to bitvec


  # TODO
  test "mixed sorts formulas":
    smt:
      let a = Bool("a")
      let b = Int("b")
      let c = Real("c")
      let d = Bv("d", 32)
      let s = Solver()
      # Mixed types
      expect SmtSyntaxError:
        # Note that `a < b` generates a compilation error as `<` is not defined for `Bool`.
        s.assert b < a
      expect SmtSyntaxError:
        s.assert b + c + d < a


  test "Define group consts":
    smt:
      let s = Solver()

      letBools a, b, c, d
      s.assert a == false
      s.assert b == c or d

      letInts e, f, g
      s.assert e == 5 * f * g

      letBvs 8, h, i
      s.assert h shl 3 == 0xff

      letReals j, k, l
      s.assert 5.2 * j / k <= l

      echo "solver =", $s


  test "ite":
    smt:
      let a = Int("a")
      let b = Int("b")
      let c = Int("c")
      let d = Int("d")
      let e = Bool("e")
      let f = Int("f")
      let g = Int("g")
      let h = Bool("h")
      let i = Bool("i")
      let j = Bool("j")
      let k = -3.14159  # float
      let l = 6.023e24  # float64
      let n = Real("n")
      let s = Solver()
      s.assert a == ite(true, 3, 5)
      s.assert b == -4
      s.assert c == ite(true, b, 2)
      s.assert d == ite(false, a, b)
      s.assert e == true
      s.assert f == ite(e, +5, -5)
      s.assert g == ite(e, a + b, c)
      s.assert h == false
      s.assert i == true or e or false
      s.assert j == ite(i, h, e)
      s.assert n == ite(false, k, l)
      
      dbg "solver=", s

      if s.check() == True3:
        let m = s.getModel()
        assert m.evalInt(a) == 3
        assert m.evalInt(c) == -4
        assert m.evalInt(d) == -4
        assert m.evalInt(f) == 5
        assert m.evalInt(g) == -1
        assert m.evalBool(j) == false
        assert m.evalReal(n) == l


  test "ite: arguments of different sorts":
    smt:
      let a = Bool("a")
      let b = Int("b")
      let c = Real("c")
      let d = Bv("d", 16)
      let e: int = 2
      let f: float = -35.04
      let s = Solver()
      s.assert a == false

      expect SmtSyntaxError:
        s.assert e == ite(a, b, c)
      expect SmtSyntaxError:
        s.assert b == ite(a, e, f)


  test "simplify":
    smt:
      let x = Int("x")
      let y = Int("y")
      let z = Real("z")
      #TODO
      dbg simplify(2 * x + 3)
      dbg simplify((x + y) * (x + y))
      dbg simplify(x ^ 2 + 2 * x * y + y ^ 2)
      dbg simplify(x * x + 2 * x * y + y * y)
      dbg simplify(x + y + 2 * x + 3)
      dbg simplify(x < y + x + 2)
      #dbg simplify(And(x + 1 >= 3, x^2 + x^2 + y^2 + 2 >= 5))