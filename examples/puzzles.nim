import unittest
import samanta


# Puzzles from SAT_SMT_by_example.pdf
# https://yurichev.com/SAT_SMT.html

suite "smt puzzles":

  test "einstein problem":
    # https://en.wikipedia.org/wiki/Zebra_Puzzle
    #
    # 1. There are five houses.
    # 2. The Englishman lives in the red house.
    # 3. The Spaniard owns the dog.
    # 4. Coffee is drunk in the green house.
    # 5. The Ukrainian drinks tea.
    # 6. The green house is immediately to the right of the ivory house.
    # 7. The Old Gold smoker owns snails.
    # 8. Kools are smoked in the yellow house.
    # 9. Milk is drunk in the middle house.
    # 10. The Norwegian lives in the first house.
    # 11. The man who smokes Chesterfields lives in the house next to the man with the fox.
    # 12. Kools are smoked in the house next to the house where the horse is kept.
    # 13. The Lucky Strike smoker drinks orange juice.
    # 14. The Japanese smokes Parliaments.
    # 15. The Norwegian lives next to the blue house.
    #
    # Now, who drinks water? Who owns the zebra?
    #
    # In the interest of clarity, it must be added that each of the five houses is painted a
    # different color, and their inhabitants are of different national extractions, own different
    # pets, drink different beverages and smoke different brands of American cigarets [sic]. One
    # other thing: in statement 6, right means your right.
    smt:
      # sorts declaration
      let
        yellow = Int("yellow")
        blue = Int("blue")
        green = Int("green")
        ivory = Int("ivory")
        red = Int("red")
        englishman = Int("Englishman")
        spaniard = Int("Spaniard")
        ukrainian = Int("Ukrainian")
        norwegian = Int("Norwegian")
        japanese = Int("Japanese")
        water = Int("water")
        tea = Int("tea")
        milk = Int("milk")
        orange_juice = Int("orange juice")
        coffee = Int("coffee")
        kools = Int("Kools")
        chesterfields = Int("Chesterfields")
        old_gold = Int("Old Gold")
        lucky_strike = Int("Lucky Strike")
        parliament = Int("Parliament")
        fox = Int("fox")
        snails = Int("snails")
        horse = Int("horse")
        dog = Int("dog")
        zebra = Int("zebra")

      let s = Solver()

      s.assert allDistinct(yellow, blue, red, green, ivory)
      s.assert allDistinct(englishman, spaniard, ukrainian, norwegian, japanese)
      s.assert allDistinct(water, tea, milk, orange_juice, coffee)
      s.assert allDistinct(kools, lucky_strike, parliament, old_gold, chesterfields)
      s.assert allDistinct(fox, snails, horse, dog, zebra)

      # Limit values
      s.assert 1 <= yellow and yellow <= 5
      s.assert 1 <= blue and blue <= 5
      s.assert 1 <= red and red <= 5
      s.assert 1 <= green and green <= 5
      s.assert 1 <= ivory and ivory <= 5

      s.assert 1 <= englishman and englishman <= 5
      s.assert 1 <= spaniard and spaniard <= 5
      s.assert 1 <= ukrainian and ukrainian <= 5
      s.assert 1 <= norwegian and norwegian <= 5
      s.assert 1 <= japanese and japanese <= 5

      s.assert 1 <= water and water <= 5
      s.assert 1 <= tea and tea <= 5
      s.assert 1 <= milk and milk <= 5
      s.assert 1 <= orange_juice and orange_juice <= 5
      s.assert 1 <= coffee and coffee <= 5

      s.assert 1 <= kools and kools <= 5
      s.assert 1 <= lucky_strike and lucky_strike <= 5
      s.assert 1 <= parliament and parliament <= 5
      s.assert 1 <= old_gold and old_gold <= 5
      s.assert 1 <= chesterfields and chesterfields <= 5

      s.assert 1 <= fox and fox <= 5
      s.assert 1 <= snails and snails <= 5
      s.assert 1 <= horse and horse <= 5
      s.assert 1 <= dog and dog <= 5
      s.assert 1 <= zebra and zebra <= 5

      # 2. The Englishman lives in the red house.
      s.assert englishman == red

      # 3. The Spaniard owns the dog.
      s.assert spaniard == dog

      # 4. Coffee is drunk in the green house.
      s.assert coffee == green

      # 5. The Ukrainian drinks tea.
      s.assert ukrainian == tea

      # 6. The green house is immediately to the right of the ivory house.
      s.assert green == ivory + 1

      # 7. The Old Gold smoker owns snails.
      s.assert old_gold == snails

      # 8. Kools are smoked in the yellow house.
      s.assert kools == yellow

      # 9. Milk is drunk in the middle house.
      s.assert milk == 3

      # 10. The Norwegian lives in the first house.
      s.assert norwegian == 1

      # 11. The man who smokes Chesterfields lives in the house next to the man with the fox.
      s.assert (chesterfields == fox + 1) or (chesterfields == fox - 1)

      # 12. Kools are smoked in the house next to the house where the horse is kept.
      s.assert (kools == horse + 1) or (kools == horse - 1)

      # 13. The Lucky Strike smoker drinks orange juice.
      s.assert lucky_strike == orange_juice

      # 14. The Japanese smokes Parliaments.
      s.assert japanese == parliament

      # 15. The Norwegian lives next to the blue house.
      s.assert (norwegian == blue + 1) or (norwegian == blue - 1)

      # Now, who drinks water? Who owns the zebra?
      s.check_model():
        echo model


  test "1959 AHSME Problems, Problem 6":
    # With the use of three different weights, namely 1 lb., 3 lb., and 9 lb., how many objects of different
    # weights can be weighed, if the objects is to be weighed and the given weights may be placed in either pan
    # of the scale? 15, 13, 11, 9, 7

    smt:
      let w1 = Int("weight 1 lb")
      let w3 = Int("weight 3 lb")
      let w9 = Int("weight 9 lb")

      let obj = Int("object position")
      let obj_w = Int("object weight")

      let left = Int("weight on left pan of the scale")
      let right = Int("weight on right pan of the scale")

      # 0 = not used, 1 = left pan of the scale, 2 = right pan of the scale
      const
        NotUsed = 0
        Left = 1
        Right = 2

      let s = Solver()

      s.assert w1 >= NotUsed and w1 <= Right
      s.assert w3 >= NotUsed and w3 <= Right
      s.assert w9 >= NotUsed and w9 <= Right

      # object is only on left or right pan
      s.assert obj >= Left and obj <= Right

      # object must weight something
      s.assert obj_w > 0

      # Left pan is the sum of all objects present
      s.assert left == ite(w1 == Left, 1, 0) + ite(w3 == Left, 3, 0) + ite(w9 == Left, 9, 0) + ite(obj == Left, obj_w, 0)
      s.assert right == ite(w1 == Right, 1, 0) + ite(w3 == Right, 3, 0) + ite(w9 == Right, 9, 0) + ite(obj == Right, obj_w, 0)

      # Both pans must weight something
      s.assert left > 0
      s.assert right > 0
      # And pans have equal weights
      s.assert left == right

      s.check_model():
        echo model
      
      #[
      var nb = 0
      while s.check() == Z3_L_TRUE:
        inc nb
        let model = s.get_model()
        echo "Solution #", nb, model, "\n"

        # Add found solution to constraints
        # TODO

      # Solutions are symetric
      echo "It is possible to weight ", nb / 2, " objects\n"
      assert nb == 13
      ]#
