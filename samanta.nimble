# Package

version       = "0.1"
author        = "Pierre Métras, based on Ico Doornekamp's work"
description   = "SMT theorem prover bindings for Nim"
license       = "MIT"
srcDir        = "src"
installExt    = @["nim"]

# Dependencies

requires "nim >= 1.2.0"

when defined(nimdistros):
  import distros
  if detectOs(Ubuntu):
    foreignDep "libz3-4"
  else:
    foreignDep "z3"


# Defines
#########

var switches = ""

# Tasks
#######

# Documentation task

proc runDoc =
  if not dirExists "build/doc":
    mkDir "build/doc"
  #exec "nim doc -o:build/doc/index.html src/samanta.nim"
  exec "nim doc --project --outDir:build/doc --docroot --index:on src/samanta.nim"


task docs, "Generate documentation":
  # TODO: Using task name `doc` does not work:
  #   options.nim(185)         get
  #   Error: unhandled exception: Can't obtain a value from a `none` [UnpackError]
  runDoc()


# Tests task

proc runTests(switches: string = "", run: bool = true) =
  if not dirExists "build/tests":
    mkDir "build/tests"
  let runFlag = if run: " -r" else: ""
  exec "nim c --outdir:build/tests " & switches & runFlag & " tests/t_sorts.nim"


task test, "Run all tests - Development mode":
  runTests()


task test_release, "Run all tests - Release mode":
  switches &= " -d:release"
  runTests switches


proc runMemory =
  #let memSwitches = " -v --leak-check=full"
  let memSwitches = " -v"
  exec "valgrind " & memSwitches & " build/tests/t_sorts"

task test_memory, "Run all tests - Check memory with valgrind":
  switches &= " --gc:arc --newRuntime --debugger:native --profiler:on --stackTrace:on --d:useMalloc"
  runTests switches, false
  runMemory()


# Examples

proc runExamples(switches: string = "") =
  if not dirExists "build/examples":
    mkDir "build/examples"
  exec "nim c --outdir:build/examples " & switches & " examples/basic.nim"
  exec "nim c --outdir:build/examples " & switches & " examples/puzzles.nim"


task examples, "Build and run all examples - Development mode":
  switches &= " -r"   # Run examples in dev mode
  runExamples switches


task examples_release, "Build all examples - Release mode":
  switches &= " -d:release"
  runTests switches


# Clean build directory

task clean, "Clean build directory":
  rmDir "build"
  echo "build directory content deleted"


# Build

proc runBuild(switches: string = "") =
  if not dirExists "build/bin":
    mkDir "build/bin"
  exec "nim c --outdir:build/bin " & switches & "src/samanta.nim"


task comp, "Build samanta - Development mode":
  # TODO: Can't use `build`. Error like for `doc`.
  runBuild()


task build_all, "Build samanta - Documentation, tests and examples":
  runBuild()
  runDoc()
  runExamples()
  runTests()

